# README #

As a user, I want to be able to see the minimum, maximum,
average temperature, and humidity for a given city and period of

## How to install it? ##


### Using Docker ###

```
docker build -t weather-backend backend/.
docker build -t weather-frontend frontend/.


docker run -d -p 8000:8000 weather-backend
docker run -d -p 8083:8083 weather-frontend
```

### Without Docker ###


Go to 'backend' the directory
```
cd backend
```

Create a new Virtualenv, Python 3 [(install virtualenv)](https://virtualenv.pypa.io/en/stable/installation/).

Activate it, install requirements and migrate the db

```
pip install -r requirements.txt
python manage.py migrate
```

and run server web
```
python manage.py runserver 0.0.0.0:8000
```

Then, frontend!

go to 'frontend' the directory
```
cd frontend
```
Install and run
```
npm install
npm start
```

### How it works ###

Go to
http://127.0.0.1:8083
and find the time!


### Next step ###

* Writing more tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
