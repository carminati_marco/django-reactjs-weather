import axios from "axios";
import { actions as notifActions } from "redux-notifications";
const { notifSend } = notifActions;

import { WeatherTypes } from "../constants/actionTypes";
import { WeatherUrls } from "../constants/urls";

function setWeathers(payload) {
  return {
    type: WeatherTypes.LIST,
    payload: payload
  };
}

function convertDate(date) {
  // convert date for querystring.
  const yyyy = date.getFullYear().toString();
  const mm = (date.getMonth()+1).toString();
  const dd = date.getDate().toString();
  const mmChars = mm.split("");
  const ddChars = dd.split("");

  return yyyy + "-" + (mmChars[1]?mm:"0"+mmChars[0]) + "-" + (ddChars[1]?dd:"0"+ddChars[0]);
}

export function getWeathers(formValues, dispatch) {
  // function to receive weathers data.

  // create query string
  const fromDate = convertDate(formValues.fromDate)
  const toDate = convertDate(formValues.toDate)

  let qs = `?from_date=${fromDate}&to_date=${toDate}&location=${formValues.location}`

  if (formValues.fromTime != null) {
    qs += `&from_time=${formValues.fromTime.toISOString().split("T")[1]}`
  }

  if (formValues.toTime != null) {
    qs += `&to_time=${formValues.toTime.toISOString().split("T")[1]}`
  }

  // get data list of Notes for user
  return axios.get(WeatherUrls.LIST + qs)
  .then(response => {
    dispatch(setWeathers(response.data.weathers))

    // check if there is an error message
    if (response.data.message) {
      dispatch(notifSend({
        message: response.data.message,
        kind: "danger",
        dismissAfter: 5000
      }))
    }
  })
  .catch(error => {
      dispatch(notifSend({
        message: error.message,
        kind: "danger",
        dismissAfter: 5000
      }))
  });  
}
