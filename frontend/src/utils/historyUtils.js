{/* Utils for browser history. */}
import createHistory from "history/createBrowserHistory";
export default createHistory();
