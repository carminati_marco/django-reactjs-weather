import React from "react";
import { Switch, Route } from "react-router-dom";

import Weather from "./weather/Weather"
import NoMatch from "./NoMatch";

const MainContent = () => (
    <div className="container-fluid">
        <Switch>
            <Route exact path="/" component={Weather}/>
            <Route component={NoMatch}/>
        </Switch>
    </div>
);

export default MainContent;
