import React, { Component } from "react";
import {ResponsiveContainer, BarChart, Bar, Brush, ReferenceLine, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from "recharts";
import { connect } from "react-redux";
import PropTypes from "prop-types";



class WeatherBarChart extends Component {
  static propTypes = {
    weathers: PropTypes.object
  }

	render () {

    const { weathers } = this.props;
    if (weathers) {
      return (
        <div>
        <ResponsiveContainer width="100%" aspect={4.0/3.0}>
          <BarChart data={weathers}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
           <XAxis dataKey="dt_txt"/>
           <YAxis/>
           <CartesianGrid strokeDasharray="3 3"/>
           <Tooltip/>
           <Legend verticalAlign="top" wrapperStyle={{lineHeight: "40px"}}/>
           <ReferenceLine y={0} stroke="#000"/>
           <Brush label="Day" dataKey="dt_txt" height={30} stroke="#d8ebff"/>
           <Bar label="min" dataKey="temp_min" fill="	#89c4ff" />
           <Bar label="Avg" dataKey="temp_avg" fill="#0080ff" />
           <Bar label="MAX" dataKey="temp_max" fill="#004589" />
          </BarChart>
        </ResponsiveContainer>
        <ResponsiveContainer width="100%" aspect={4.0/3.0}>
          <BarChart data={weathers}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
             <XAxis dataKey="dt_txt"/>
             <YAxis/>
             <CartesianGrid strokeDasharray="3 3"/>
             <Tooltip/>
             <Legend verticalAlign="top" wrapperStyle={{lineHeight: "40px"}}/>
             <ReferenceLine y={0} stroke="#000"/>
             <Brush dataKey="dt_txt" height={30} stroke="#0080ff"/>
             <Bar dataKey="humidity" fill="#004589" />
          </BarChart>
        </ResponsiveContainer>
        </div>
      )
    } else {
      return (<div></div>)
    }
  }
}

function mapStateToProps(state) {
    return {
        weathers: state.weather.weathers,
    }
}
export default connect(mapStateToProps)(WeatherBarChart);
