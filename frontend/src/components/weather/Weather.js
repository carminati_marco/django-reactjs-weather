import React, { Component } from "react";

import WeatherForm from "./WeatherForm"
import WeatherTable from "./WeatherTable"
import WeatherBarchart from "./WeatherBarchart"


class Weather extends Component {

    render() {
          return (
            <div className="col col-sm-12 mt-3 p-2">
              <h4 className="text-md-left">Weather</h4>
              <WeatherForm />
              <hr />
              <WeatherTable />
              <hr />
              <WeatherBarchart />
              <div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from
              <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
              title="Creative Commons BY 3.0" rel="noopener noreferrer" target="_blank">CC 3.0 BY</a></div>
            </div>
          )
    }
}

export default Weather;
