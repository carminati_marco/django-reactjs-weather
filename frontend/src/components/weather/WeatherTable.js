import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { TablePagination } from "react-pagination-table";

const Header = ["Date", "Hour", "Humidity", "°C Avg", "°C Min", "°C MAX "];

class WeatherTable extends Component {
    static propTypes = {
      weathers: PropTypes.object
    }

    render() {
        const weathers = this.props.weathers;

        if (weathers) {
          return (
            <div className="justify-content-left col col-sm-12 mt-12 p-12">
            <TablePagination
              headers={ Header }
              data={ weathers }
              columns="day.hour.humidity.temp_avg_str.temp_min_str.temp_max_str"
              perPageItemCount={ 5 }
              totalCount={ weathers.length }
              arrayOption={ [["day","temp_avg", " "]] }
          />
            </div>
          )
        } else {
          return (<div></div>)
        }
    }
}

function mapStateToProps(state) {
    return {
        weathers: state.weather.weathers,
    }
}
export default connect(mapStateToProps)(WeatherTable);
