import React, { Component } from "react";
import { reduxForm, Field, propTypes } from "redux-form";

import { TextField, DatePicker, TimePicker } from "redux-form-material-ui"

import { getWeathers } from "../../actions/weatherActions";

function convertDate(date) {
  // convert date for check data.
  const yyyy = date.getFullYear().toString();
  const mm = (date.getMonth()+1).toString();
  const dd = date.getDate().toString();
  const mmChars = mm.split("");
  const ddChars = dd.split("");

  return yyyy + "-" + (mmChars[1]?mm:"0"+mmChars[0]) + "-" + (ddChars[1]?dd:"0"+ddChars[0]);
}

const validate = values => {
  const errors = {}
  const requiredFields = [
    "location",
    "fromDate",
    "toDate"
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = "This field is required"
    }
  })
  // check date and time
  if (values.fromDate != null && values.toDate != null) {
    const fromDate = convertDate(values.fromDate)
    const toDate = convertDate(values.toDate)
    
    // check if date are equals =>  check hours.
    if (fromDate == toDate) {
      // check if times are selected.
      if (values.fromTime != null && values.toTime != null) {
        if (values.fromTime.getTime() > values.toTime.getTime()) {
           errors.toTime = "This mustn't be lower than From Time!!!!"
        }
      }
    // check if fromdate > todate.
  } else if (values.fromDate > values.toDate) {
       errors.toDate = "This mustn't be lower than From Date!!!!"
    }
  }
  return errors
}

class WeatherForm extends Component {

    static propTypes = {
        ...propTypes
    };

    render() {
        const { handleSubmit } = this.props;

        return (
            <div className="justify-content-left col col-sm-12 mt-12 p-12">
              <hr/>
              <form className="col col-sm-6 p-12" onSubmit={handleSubmit} >

                    <div>
                        <Field name="location" floatingLabelText="Location" component={TextField}
                               type="text" />
                    </div>

                    <div>
                      <Field name="fromDate" component={DatePicker} locale="en-US" format={null}
                      hintText="From date" />
                      <Field name="fromTime" component={TimePicker} format={null} hintText="From time"/>

                      <Field name="toDate" component={DatePicker} locale="en-US" format={null}
                      hintText="To date" />
                      <Field name="toTime" component={TimePicker} format={null} hintText="To time"/>
                    </div>

                    <div className="mt-3" >
                        <button action="submit" className="btn btn-primary mt-2">Send</button>
                    </div>
              </form>
            </div>
        )
    }
}

export default reduxForm({
    form: "weather",
    validate,
    onSubmit: getWeathers
})(WeatherForm);
