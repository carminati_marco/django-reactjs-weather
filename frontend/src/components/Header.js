import React, { Component } from "react";
import { Link } from "react-router-dom";

class Header extends Component {

    renderLinks() {
            return (
                [
                  <li className="nav-item" key="home">
                      <Link className="nav-link" to="/">Weather</Link>
                  </li>,
                ]
            );
        }


    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">

                <Link to="/" className="navbar-brand">
                  <img src="/src/images/cloud.png" />
                </Link>
                <ul className="navbar-nav">
                    {this.renderLinks()}
                </ul>
            </nav>
        )
    }
}

export default Header;
