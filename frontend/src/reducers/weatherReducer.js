import { WeatherTypes } from "../constants/actionTypes";

export default function(state = {}, action) {
    switch(action.type) {
        case WeatherTypes.LIST:
            return { ...state, weathers: action.payload};
    }
    return state;
}
