import {combineReducers} from "redux";
import { reducer as formReducer } from "redux-form";
import { reducer as notifReducer } from "redux-notifications";

{/* create reducer for each models */}
import weatherReducer from "./weatherReducer";

const rootReducer = combineReducers({
    form: formReducer,
    notifs: notifReducer,
    weather: weatherReducer
});

export default rootReducer;
