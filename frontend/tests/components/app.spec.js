import {shallow} from "enzyme";
import App from "../../src/components/App";
import WeatherForm from "../../src/components/weather/WeatherForm"
import WeatherTable from "../../src/components/weather/WeatherTable"
import WeatherBarchart from "../../src/components/weather/WeatherBarchart"

describe("App", () => {
    let component;

    beforeEach(() => {
        component = shallow(<App/>);
    });

    it("renders something", () => {
        expect(component).to.exist;
    });
});

describe("WeatherForm", () => {
    let component;

    beforeEach(() => {
        component = shallow(<WeatherForm/>);
    });

    it("renders something", () => {
        expect(component).to.exist;
    });
});
