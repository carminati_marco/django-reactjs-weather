WS_URI = "http://api.openweathermap.org/data/2.5/forecast?q=%(location)s&APPID=d8a883ea8aec0ad31302e1ddbbebdf98"

# Difference KELVIN-CELSIUS
KELVIN = 273.15

DATETIME_NOT_VALID = "Date/Time not valid!"
NO_DATA_FOUND = "No data found!"

TIME_FORMAT = "%H:%M:%S.%fZ"
DATE_FORMAT = "%Y-%m-%d"
