# Create your views here.
from datetime import datetime, time

import requests
from rest_framework.response import Response
from rest_framework.views import APIView

from .constants import WS_URI, DATETIME_NOT_VALID, TIME_FORMAT, DATE_FORMAT
from .utils import cast_weather_data


class WeatherList(APIView):

    def get_fromto_date(self, request):
        """
        create from date and to date.
        :param request:
        :return:
        """
        from_date = request.GET.get("from_date", "")
        to_date = request.GET.get("to_date", "")

        try:
            from_date = datetime.strptime(from_date, DATE_FORMAT).date()
            to_date = datetime.strptime(to_date, DATE_FORMAT).date()
        except ValueError:
            raise ValueError

        from_time = request.GET.get("from_time", "")
        if from_time:
            try:
                from_time = datetime.strptime(from_time, TIME_FORMAT).time()
            except ValueError:
                raise ValueError
        else:
            from_time = time(0, 0)

        to_time = request.GET.get("to_time", "")
        if to_time:
            to_time = datetime.strptime(to_time, TIME_FORMAT).time()
        else:
            try:
                to_time = time(23, 59)
            except ValueError:
                raise ValueError

        return datetime.combine(from_date, from_time), datetime.combine(to_date, to_time)

    def get(self, request, format=None):

        # init variable
        location = request.GET.get("location", "")
        try:
            from_date, to_date = self.get_fromto_date(request)
        except ValueError:
            return Response(dict(weathers=[], cod=501, message=DATETIME_NOT_VALID))

        # compose + call ws
        ws = WS_URI % dict(location=location)
        r = requests.get(ws)

        # cast request
        content = cast_weather_data(r.json(), from_date, to_date)
        return Response(content)
