import json
from datetime import datetime, timedelta

from django.urls import reverse
from rest_framework.test import APITestCase

from weather.constants import DATETIME_NOT_VALID, NO_DATA_FOUND, TIME_FORMAT, DATE_FORMAT


class WeatherListCreateAPIViewTestCase(APITestCase):
    url = reverse("weather:list")

    def test_get(self):
        location = "London"
        from_date = datetime.today().strftime(DATE_FORMAT)
        to_date = (datetime.today() + timedelta(days=1)).strftime(DATE_FORMAT)

        response = self.client.get(self.url, {"location": location, "from_date": from_date, "to_date": to_date})
        self.assertEqual(200, response.status_code)

    def test_get_time(self):
        location = "London"
        from_date = datetime.today().strftime(DATE_FORMAT)
        to_date = (datetime.today() + timedelta(days=1)).strftime(DATE_FORMAT)

        from_time = datetime.today().strftime(TIME_FORMAT)
        to_time = (datetime.today() + timedelta(days=1)).strftime(TIME_FORMAT)

        response = self.client.get(self.url, {"location": location, "from_date": from_date, "to_date": to_date,
                                              "from_time": from_time, "to_time": to_time})
        self.assertEqual(200, response.status_code)

    def test_get_with_wrong_format_value(self):
        location = "London"
        from_date = datetime.today().strftime("%y-%m-%d")
        to_date = (datetime.today() + timedelta(days=1)).strftime("%y-%m-%d")

        response = self.client.get(self.url, {"location": location, "from_date": from_date, "to_date": to_date})
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content)
        self.assertTrue(content["message"] == DATETIME_NOT_VALID)

    def test_get_with_TIME_wrong_format_value(self):
        location = "London"
        from_date = datetime.today().strftime(DATE_FORMAT)
        to_date = (datetime.today() + timedelta(days=1)).strftime(DATE_FORMAT)

        from_time = datetime.today().strftime("%H:%M:%S")
        to_time = (datetime.today() + timedelta(days=1)).strftime(TIME_FORMAT)

        response = self.client.get(self.url, {"location": location, "from_date": from_date, "to_date": to_date,
                                              "from_time": from_time, "to_time": to_time})
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content)
        self.assertTrue(content["message"] == DATETIME_NOT_VALID)

    def test_get_with_yesterday_values(self):
        location = "London"
        from_date = (datetime.today() - timedelta(days=1)).strftime(DATE_FORMAT)
        to_date = (datetime.today() - timedelta(days=2)).strftime(DATE_FORMAT)

        response = self.client.get(self.url, {"location": location, "from_date": from_date, "to_date": to_date})
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content)
        self.assertTrue(content["message"] == NO_DATA_FOUND)

    def test_get_with_wrong_city(self):
        location = "Loonon"
        from_date = (datetime.today() - timedelta(days=1)).strftime(DATE_FORMAT)
        to_date = (datetime.today() - timedelta(days=2)).strftime(DATE_FORMAT)

        response = self.client.get(self.url, {"location": location, "from_date": from_date, "to_date": to_date})
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content)
        self.assertTrue(content["message"] == "city not found")  # received from API/REST
