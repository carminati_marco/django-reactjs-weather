from datetime import datetime

from .constants import KELVIN, NO_DATA_FOUND


def cast_weather_data(r_json, from_date, to_date):
    """
    Cast data received from API.
    """

    # init return values.
    weathers = []
    cod, message = None, None

    # iter json list if found.
    if "list" in r_json:
        for l in r_json["list"]:

            # get data.
            dt_txt = datetime.strptime(l["dt_txt"], "%Y-%m-%d %H:%M:%S")

            if from_date < dt_txt < to_date:
                day = dt_txt.strftime("%m/%d")
                hour = dt_txt.strftime("%H:%M")

                temp_min = l["main"]["temp_min"] - KELVIN
                temp_max = l["main"]["temp_max"] - KELVIN
                temp_avg = (temp_min + temp_max) / 2
                humidity = l["main"]["humidity"]

                weathers.append(dict(dt_txt=dt_txt.strftime("%m/%d %H:%M"), day=day, hour=hour,
                                     temp_min=temp_min, temp_max=temp_max, temp_avg=temp_avg,
                                     temp_min_str="%.2f" % temp_min, temp_max_str="%.2f" % temp_max,
                                     temp_avg_str="%.2f" % temp_avg,
                                     humidity=humidity))

    else:
        # I obtained an exception from ws like {"cod": "404", "message": "city not found"}
        cod = r_json["cod"] if "cod" in r_json else ""
        message = r_json["message"] if "message" in r_json else ""

    # check if I hadn't an exception and I have data.
    if cod is None and len(weathers) == 0:
        message = NO_DATA_FOUND

    return dict(weathers=weathers, cod=cod, message=message)
