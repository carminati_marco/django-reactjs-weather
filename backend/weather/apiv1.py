from django.urls import path

from .views import WeatherList

"""
Pattern for API v. 1.
"""

app_name = 'weather'
urlpatterns = [
    path(r'list/', WeatherList.as_view(), name="list"),
]
